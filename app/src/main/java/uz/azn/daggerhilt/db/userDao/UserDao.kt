package uz.azn.daggerhilt.db.userDao

import androidx.lifecycle.LiveData
import androidx.room.*
import uz.azn.daggerhilt.db.model.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userEntity: UserEntity)

    @Update
    suspend fun update(userEntity: UserEntity)

    @Delete
    suspend fun delete(userEntity: UserEntity)

    @Query("SELECT * FROM user_table")
   fun getAllUser(): LiveData<List<UserEntity>>
}