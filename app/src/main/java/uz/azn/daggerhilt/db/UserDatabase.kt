package uz.azn.daggerhilt.db

import androidx.room.Database
import androidx.room.RoomDatabase
import uz.azn.daggerhilt.db.model.UserEntity
import uz.azn.daggerhilt.db.userDao.UserDao

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao

}