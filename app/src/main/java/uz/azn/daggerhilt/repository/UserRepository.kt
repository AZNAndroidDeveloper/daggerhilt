package uz.azn.daggerhilt.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import uz.azn.daggerhilt.db.model.UserEntity
import uz.azn.daggerhilt.db.userDao.UserDao
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userDao: UserDao
) {
    val getAllUserData = userDao.getAllUser()

    suspend fun insertUser(userEntity: UserEntity) = withContext(Dispatchers.IO) {
        userDao.insert(userEntity)
    }

    suspend fun updateUser(userEntity: UserEntity) = withContext(Dispatchers.IO) {
        userDao.update(userEntity)
    }

    suspend fun deleteUser(userEntity: UserEntity) = withContext(Dispatchers.IO) {
        userDao.delete(userEntity)
    }
}
// Inject anotatsiyasi @providerAnotatsiyasiga qarab olib beradi