package uz.azn.daggerhilt.intro.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.daggerhilt.databinding.RowItemBinding
import uz.azn.daggerhilt.db.model.UserEntity

class RecyclerViewAdapter(private val onItemClick: (UserEntity) -> Unit) :
    RecyclerView.Adapter<RecyclerViewAdapter.RowViewHolder>() {

    private val elements = mutableListOf<UserEntity>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowViewHolder {
        return RowViewHolder(
            RowItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: RowViewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    inner class RowViewHolder(
        private val binding: RowItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(element: UserEntity) {
            with(binding) {
                tvCountry.text = element.country
                tvUser.text = element.name
            }
            itemView.setOnClickListener {
                onItemClick(element)
            }

        }
    }

    fun setData(element: List<UserEntity>) {
        elements.apply {clear(); addAll(element) }
    }
}