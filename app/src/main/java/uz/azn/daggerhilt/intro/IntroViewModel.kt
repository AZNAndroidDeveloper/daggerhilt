package uz.azn.daggerhilt.intro

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import uz.azn.daggerhilt.db.model.UserEntity
import uz.azn.daggerhilt.repository.UserRepository

class IntroViewModel @ViewModelInject constructor(
    private val repository: UserRepository
) : ViewModel() {

    val getAllUser: LiveData<List<UserEntity>> = repository.getAllUserData

    fun insert(userEntity: UserEntity) = viewModelScope.launch {
        repository.insertUser(userEntity)
    }

    fun update(userEntity: UserEntity) = viewModelScope.launch {
        repository.updateUser(userEntity)
    }

    fun delete(userEntity: UserEntity) = viewModelScope.launch {
        repository.deleteUser(userEntity)
    }


}