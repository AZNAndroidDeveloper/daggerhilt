package uz.azn.daggerhilt.intro

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import uz.azn.daggerhilt.R
import uz.azn.daggerhilt.databinding.DialogItemBinding
import uz.azn.daggerhilt.databinding.FragmentIntroBinding
import uz.azn.daggerhilt.db.model.UserEntity
import uz.azn.daggerhilt.intro.adapter.RecyclerViewAdapter

@AndroidEntryPoint
class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private val recycleAdapter by lazy {
        RecyclerViewAdapter(
            onItemClick = {
                onItemClicked(it)
            }
        )
    }
    private val viewModel: IntroViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)

        viewModel.getAllUser.observe(requireActivity(), Observer {
            recycleAdapter.setData(it)
        })

        with(binding) {
            recyclerView.apply {
                adapter = recycleAdapter
                layoutManager = LinearLayoutManager(requireContext())
            }
            btnSave.setOnClickListener {
                if (etCountry.text.isNotEmpty() && etUser.text.isNotEmpty()) {
                    insertData(UserEntity(etUser.text.toString(), etCountry.text.toString()))
                    etCountry.text.clear()
                    etUser.text.clear()
                }

            }
        }
    }


    private fun onItemClicked(userEntity: UserEntity) {
        val dialogBinding =
            DialogItemBinding.inflate(
                LayoutInflater.from(requireContext()), null, false
            )
        val builder = AlertDialog.Builder(requireContext())
        builder.setView(dialogBinding.root)

        val dialog = builder.show()
        dialog.show()
        with(dialogBinding) {
            etCountry.setText(userEntity.country)
            etUser.setText(userEntity.name)

            btnUpdate.setOnClickListener {
                userEntity.apply {
                    etUser.text.toString()
                    etCountry.text.toString()
                }
                update(userEntity)
                dialog.dismiss()
            }
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
            btnDelete.setOnClickListener {
                viewModel.delete(userEntity)
                dialog.dismiss()
            }
        }

        Toast.makeText(
            requireContext(),
            "${userEntity.country} ${userEntity.name}",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun insertData(userEntity: UserEntity) {
        viewModel.insert(userEntity)
    }

    private fun update(userEntity: UserEntity) {
        viewModel.update(userEntity)
    }
}