package uz.azn.daggerhilt.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import uz.azn.daggerhilt.db.UserDatabase
import uz.azn.daggerhilt.db.userDao.UserDao
import uz.azn.daggerhilt.repository.UserRepository
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class) // bu anatatsiya qaysi fragment va activitydan foydalanishda ishlatiladi

class AppModule {

    @Singleton // bu userni birmarta yaratib beradi va  shu boyicha men yaratib olaman
    @Provides // bu kuzatib yuradi qayerda bolsa usha yerda chaqiradi
    fun providerUserDatabase(@ApplicationContext context: Context): UserDatabase =
        Room.databaseBuilder(context, UserDatabase::class.java, "user_table")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    fun provideUserDao(userDatabase: UserDatabase):UserDao = userDatabase.userDao()

    @Provides
    fun providerUserRepository(userDao: UserDao):UserRepository = UserRepository(userDao)
}